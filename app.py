#!python3
# -*- coding: utf-8 -*-
import time
from soundtouch_controller.soundtouch_controller import Zone, Speaker
import logging
import os
import json

# APP Version
app_version = "0.91"

logger = logging.getLogger(__name__)
logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=os.getenv("LOGLEVEL", "INFO"))

def main():
    # Check if ZONECONFIG Environment Variable has been set
    config = os.getenv("ZONECONFIG", None)

    if config:
        logger.info("[~] Soundtouch Controller Version {}".format(app_version))
        logger.info("[+] Config loaded from Environment")
        logger.info(config)
        zoneconfig = json.loads(config)
        logger.info(zoneconfig)
    else:
        config_path = os.getenv("CONFIG_PATH", "/config/zoneconfig.json")
        logger.info("[+] Config Path: {}".format(config_path))
        zoneconfig = {}
        try:
            with open(config_path) as f:
                zoneconfig = json.load(f)
        except Exception as e:
            raise ValueError("[x] No config file found: {}".format(e))

    zone = Zone(zoneconfig=zoneconfig)
    while True:
        if zone:
            if zone._zoneconfig["bypass"]:
                logger.info("Bypassing with Master {}".format(zone._zoneconfig["master"]))
                # Static Master
                zone.master = Speaker(zone._zoneconfig["master"])
                zone.run()
            else:
                if not zone._master:
                    logger.warning("[x] No Master found, re-discovering")
                    zone._discover()
                else:
                    zone.run()
        else:
            logger.error("[x] No Zone loaded. Dying...")

if __name__ == "__main__":
    main()
