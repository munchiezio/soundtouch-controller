#!python3
# -*- coding: utf-8 -*-
from libsoundtouch.device import SoundTouchDevice
from libsoundtouch.utils import Source
from zeroconf import ServiceBrowser, Zeroconf
import time
import socket
import datetime
import logging
import os
import pprint

# https://samhobbs.co.uk/2016/01/connect-bose-soundtouch-10-wifi-using-linux-telnet
# https://pastebin.com/EXCQXmfY
# scm sys reboot
# scm sys wakeup 0
# sys reboot

pp = pprint.PrettyPrinter(indent=4)
logger = logging.getLogger(__name__)
logging.basicConfig(
    format='%(asctime)s - %(levelname)s - %(message)s',
    level=os.getenv("LOGLEVEL", "INFO"))

class Speaker(SoundTouchDevice):
    def _is_master(self):
        self._zone_status = self._master.zone_status()
        if self._zone_status:
            if self._zone_status.is_master:
                return True
        return False


class Zone:
    def __init__(self, zoneconfig=None):
        self._master = None
        self._slaves = []

        self._valid_sources = ["SPOTIFY", "BLUETOOTH", "AIRPLAY", "UPDATE", "INVALID_SOURCE", "AUX", "STANDBY"]

        self._zoneconfig = {}
        self.config(zoneconfig)

        self._discover()

        # Update status
        #self._status = self.Status()
    
    def _discover(self):
        r = Zeroconf()
        ServiceBrowser(r, "_soundtouch._tcp.local.",
                       handlers=[self._discoveryHandler])
        time.sleep(3)

    def _discoveryHandler(self, zeroconf, service_type, name, state_change):
        info = zeroconf.get_service_info(service_type, name)
        speaker = Speaker(socket.inet_ntoa(info.address))

        if not self._master and speaker != self._master:
            self._master = speaker
        elif speaker not in self._slaves:
            self._slaves.append(speaker)
        
        logger.info("[+] Added speaker '{}' ({})".format(speaker.config.name, socket.inet_ntoa(info.address)))

        return speaker

    def _create_zone(self):
        self._zone_status = self._master.zone_status()

        if not self._zone_status:
            if self._master:
                if self._slaves and len(self._slaves) > 0:
                    try:
                        self._master.create_zone(self._slaves)
                        logger.info("[+] Created Zone: {} + {} Slaves"
                                    .format(self._master.config.name,len(self._slaves)))
                        return True
                    except Exception as e:
                        logger.warning("[!] Failed to create Zone: {}".format(e))
                        return False
                else:
                    logger.warning("[!] Failed to create Zone: No slaves defined")
                    return False
            else:
                logger.warning("[!] Failed to create Zone: No Master defined")
                return False
        return False

    @property
    def zoneconfig(self):
        return self._zoneconfig

    @zoneconfig.setter
    def zoneconfig(self, zoneconfig):
        if not zoneconfig:
            logger.error("No zoneconfig given")
            raise ValueError("No Zoneconfig given")
        self._zoneconfig = zoneconfig

    def _setVolume(self, volume):
        _volume = self._status['volume']['actual']

        if volume != _volume:
            self._master.set_volume(self._zoneconfig["volume"])
            logger.info("[+] Volume: {} (was:{})".format(self._zoneconfig["volume"], self._status['volume']['actual']))
            time.sleep(5)
        else:
            logger.info("[=] Volume: {}".format(self._zoneconfig["volume"]))
        return True
        
    def _setShuffle(self, shuffle):
        _shuffle = self._status['playing']['shuffle_setting']
        if _shuffle != shuffle:
            if shuffle == "SHUFFLE_ON":
                self._master.shuffle(True)
            elif shuffle == "SHUFFLE_OFF":
                self._master.shuffle(False)
            
            logger.info("[+] Shuffle: {} (was: {})".format(shuffle, self._status['playing']['shuffle_setting']))
            time.sleep(5)
        else:
            logger.info("[=] Shuffle: {}".format(shuffle))



    def Status(self):
        logger.info("[*] Updating Status")
        self._status = self._master.status()
        self._playing = self._status.content_item
        self._zone_status = self._master.zone_status()
        self._volume = self._master.volume()
        status = {
            "playing": {
                "artist": self._status.artist,
                "track": self._status.track,
                "track_id": self._status.track_id,
                "station_name": self._status.station_name,
                "station_location": self._status.station_name,
                "album": self._status.album,
                "image": self._status.image,
                "source": self._status.source,
                "duration": self._status.duration,
                "repeat_setting": self._status.repeat_setting,
                "shuffle_setting": self._status.shuffle_setting,
                "position": self._status.position,
                "play_status": self._status.play_status,
                "description": self._status.description,
                "stream_type": self._status.stream_type,
            },
            "volume": {
                "actual": self._volume.actual,
                "muted": self._volume.muted,
                "target": self._volume.target
            },
            "zone_status": {
                "master": None,
                "slaves": [],
            }
        }

        if self._playing:
            status['playing']["playlist"] = {
                "name": self._playing.name,
                "location": self._playing.location,
                "source_account": self._playing.source_account,
                "type": self._playing.type,
                "source": self._playing.source
            }
        else:
            status['playing']["playlist"] = {
                "name": None,
                "location": None,
                "source_account": None,
                "type": None,
                "source": None
            }

        # Master
        status['zone_status']["master"] = {
            "name": self._master.config.name,
            "id": self._master.config.device_id,
            "ip": self._master.config.device_ip,
            "mac": self._master.config.mac_address,
            "type": self._master.config.type,
            "variant": self._master.config.variant,
            "variant_mode": self._master.config.variant_mode,
            "module_type": self._master.config.module_type,
            "country_code": self._master.config.country_code,
            "account_uuid": self._master.config.account_uuid,
            "region_code": self._master.config.region_code,
            "components": []
        }
        for component in self._master.config.components:
            citem = {
                "category": component.category,
                "serial_number": component.serial_number,
                "software_version": component.software_version
            }
            status['zone_status']["master"]["components"].append(citem)

        # Slaves
        for slave in self._slaves:
            item = {
                "name": slave.config.name,
                "id": slave.config.device_id,
                "ip": slave.config.device_ip,
                "mac": slave.config.mac_address,
                "type": slave.config.type,
                "variant": slave.config.variant,
                "variant_mode": slave.config.variant_mode,
                "module_type": slave.config.module_type,
                "country_code": slave.config.country_code,
                "account_uuid": slave.config.account_uuid,
                "region_code": slave.config.region_code,
                "components": []
            }

            for component in slave.config.components:
                citem = {
                    "category": component.category,
                    "serial_number": component.serial_number,
                    "software_version": component.software_version
                }
                item["components"].append(citem)

            status['zone_status']['slaves'].append(item)
        return status

    def config(self, zoneconfig):
        if not zoneconfig:
            raise ValueError("No Zoneconfig given")
        self._zoneconfig = zoneconfig

    def _find_slot(self):
        now = datetime.datetime.now()
        if len(self._zoneconfig["dow"]) == 1:
            weekday = 0
        else:
            weekday = now.weekday()

        # Same config for every day
        slots = self._zoneconfig["dow"][weekday]
        for slot in slots:
            if slot['start'] == "*" and slot['end'] == "*":
                # Default, apply and continue
                return slot

            start_time = slot['start'].split(":")
            end_time = slot['end'].split(":")

            start = datetime.datetime.now().replace(
                hour=int(start_time[0]), minute=int(start_time[1]))
            end = datetime.datetime.now().replace(
                hour=int(end_time[0]), minute=int(end_time[1]))
            if now >= start and now <= end:
                return slot

        return None

    def _parse_track(self, track):
        return track.split(":")[-1]

    def _apply_slot(self, slot):
        buffering = 0

        logger.info("[~] Master: {}".format(self._status['zone_status']["master"]['name']))
        #_slaves = ', '.join("{!s}".format(slave['name']) for slave in self._status['zone_status']["slaves"])
        logger.info("[~] Slaves: {}".format(', '.join("{!s}".format(slave['name']) for slave in self._status['zone_status']["slaves"])))

        unwanted_states = ["STOP_STATE", "PAUSE_STATE", "STANDBY", "None", "UPDATE", "BUFFERING_STATE"]

        if slot:
            # Firmware Update?
            if self._status['playing']['source'] == "UPDATE":
                # Do nothing
                logger.warning("[!] Firmware Update in Progress. Idling for {}s".format(self._zoneconfig['interval']))
                time.sleep(self._zoneconfig['interval'])
                return 60

            # INVALID_SOURCE?
            if self._status['playing']['source'] == "INVALID_SOURCE":
                # Kill it with Fire
                logger.warning("[!] INVALID_SOURCE encountered. Powering Off".format(self._zoneconfig['interval']))
                #self._master.power_off()
                #return 60

            # External Source?
            if self._status['playing']['source'] in ["BLUETOOTH", "AUX"]:
                # We assume this is wanted
                logger.info("[!] External source ({}) discovered".format(self._status['playing']['source']))

                # Reset Source
                if self._zoneconfig['override_ext']:
                    logger.warning("[!] Overriding current status")
                else:
                    # Something is playing. Let it be
                    logger.info("[!] Nothing to do")
                    return 10
            
            # Source defined? 
            # Source not "STOP_STATE", "PAUSE_STATE", "STANDBY", "None", "UPDATE"? 
            # Status not "STOP_STATE", "PAUSE_STATE", "STANDBY", "None", "UPDATE"?
            if (slot['source'] and slot['source'] not in unwanted_states and slot['status'] not in unwanted_states):
                playlist_uri = self._zoneconfig[slot['source']]['playlists'][slot['playlist']]
            
            # Buffering?
            if self._status['playing']['play_status'] == "BUFFERING_STATE":
                logger.info("[~] Buffering stuff")
                buffering += 1
                return 10

            if slot['status'] == "PLAY_STATE":
                # Set Playlist URI
                playlist_uri = self._zoneconfig[slot['source']]['playlists'][slot['playlist']]

                # If we're here, we definitely want to have the speaker play our stuff. So check if it's in stand-by
                # and power it up if so
                if self._status['playing']['source'] == "STANDBY":
                    logger.info("[+] Waking from Standby")
                    self._master.power_on()
                    return 10

                # Check Volume
                if 'volume' in slot:
                    self._setVolume(slot['volume'])
                else:
                    self._setVolume(self._zoneconfig['volume'])
                
                # Check Shuffle
                self._setShuffle(self._zoneconfig['shuffle'])

                # Correct status? i.e. PLAY_STATE == PLAY_STATE?
                if self._status['playing']['play_status'] == slot['status']:
                    logger.info("[=] Status: {}".format(slot['status']))

                    # Correct source? i.e. SPOTIFY == SPOTIFY?
                    if self._status['playing']['source'] == slot['source']:
                        logger.info("[=] Source: {}".format(slot['source']))
                        
                        # Correct playlist? i.e. indicate == indicate?
                        if self._status['playing']['playlist']['location'] == playlist_uri:
                            logger.info("[=] Playlist: {}".format(playlist_uri))
                        
                            # Nothing to do
                            logger.info("[=] Playing '{}' from '{}' (Playlist: {}) on {} ({} from {} seconds)".format(self._status['playing']['track'], self._status['playing']['artist'], self._status['playing']['playlist']['name'], self._status['playing']['source'], self._status['playing']['position'], self._status['playing']['duration']))
                            return 60

                if slot['source'] == "SPOTIFY":
                    user_id = self._zoneconfig[slot['source']]['user_id']
                    logger.info("[>] Status: {} (was: {})".format(slot['status'], self._status['playing']['play_status']))
                    logger.info("[>] Source: {} (was: {})".format(slot['source'], self._status['playing']['source']))
                    logger.info("[>] Playlist: {} (was: {})".format(playlist_uri, self._status['playing']['playlist']['location']))
                    time.sleep(2)
                    
                    logger.info("[>] Trying to start playback")
                    try:
                        self._master.play_media(Source.SPOTIFY, playlist_uri, user_id)
                        logger.info("[>] Waiting 30s before updating status")
                        time.sleep(30)

                        # Update Status
                        self._status = self.Status()
                        logger.info("[>] Playing '{}' from '{}' (Playlist: {}) on {} ({} from {} seconds)".format(self._status['playing']['track'], self._status['playing']['artist'], self._status['playing']['playlist']['name'], self._status['playing']['source'], self._status['playing']['position'], self._status['playing']['duration']))
                        return 60
                    except Exception as e:
                        logger.warning("[!] Something went wrong: {}".format(e))

            elif (slot['status'] in unwanted_states and self._status['playing']['source'] not in unwanted_states):
                logger.info('[x] Status: {} (was: {})'.format(slot['status'], self._status['playing']['play_status']))
                logger.info("[x] Source: {} (was: {})".format(slot['source'], self._status['playing']['source']))

                if self._status['playing']['play_status'] != "PAUSE_STATE":
                    logger.warning("[x] Pausing")
                    self._master.pause()
                    logger.info("[*] Waiting 5s")
                    time.sleep(5)
                return 10

            # Semms everything is as it has to be
            logger.info('[~] Status: {}'.format(self._status['playing']['play_status']))
            logger.info("[~] Source: {}".format(self._status['playing']['source']))
            logger.info("[~] Nothing to do")
            return 10

        else:
            raise ValueError("No slot defined")
        return False

    def run(self):
        # Update Status
        self._status = self.Status()

        if not self._master:
            logger.warning("[!] No Master found. Skipping this turn")
            return False

        # Zoning Stuff
        if self._zoneconfig['zoning']:        
            try:
                self._create_zone()
            except Exception as e:
                logger.warning("[!] Zoning failed: {}".format(e))
        else:
            logger.info("[~] Zoning disabled")

        slot = self._find_slot()
        try:
            sleep = self._apply_slot(slot)
            if sleep:
                try:
                    timer = int(sleep)
                except Exception as e:
                    timer = self.zoneconfig['interval']
                logger.info("[*] Waiting {}s".format(timer))
                time.sleep(timer)
            else:
                time.sleep(self.zoneconfig['interval'])
        except Exception as e:
            logger.error("[!] Couldn't apply slot: {}".format(e))
        return True
