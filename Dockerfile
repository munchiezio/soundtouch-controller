FROM python:3.7-alpine
LABEL maintainer="Fabian Peter <fabian@rootless.sh>"

#ENV INSTALL_PATH /app
#RUN mkdir -p $INSTALL_PATH
WORKDIR /app

ENV TZ=Europe/Berlin

COPY requirements.txt requirements.txt
RUN apk add -U tzdata && \
    cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN apk add --update --no-cache --virtual .build-deps \
        g++ \
        python-dev \
        libxml2 \
        libxml2-dev && \
    apk add libxslt-dev && \
    pip install --no-cache-dir -r requirements.txt && \
    apk del .build-deps
COPY . .
#RUN pip3 install -r requirements.txt

#EXPOSE 8000
#HEALTHCHECK CMD wget -q -O /dev/null http://localhost:8000/healthy || exit 1

CMD [ "python3", "app.py" ]
#CMD gunicorn -t 120 -b 0.0.0.0:8000 --access-logfile - "soundtouch_controller.app:create_app()"
#CMD ["gunicorn", "-c", "python:config.gunicorn", "soundtouch_controller.app:create_app()"]
